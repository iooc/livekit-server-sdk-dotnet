﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livekit
{
    public class VideoGrant
    {
        /// <summary>
        /// permission to create a room
        /// </summary>
        public bool? RoomCreate { get; set; }//?: boolean;

        /// <summary>
        /// permission to join a room as a participant, room must be set
        /// </summary>
        public bool? RoomJoin { get; set; }//?: boolean;

        /// <summary>
        /// permission to list rooms
        /// </summary>
        public bool? RoomList { get; set; }//?: boolean;

        /// <summary>
        /// permission to start a recording
        /// </summary>
        public bool? RoomRecord { get; set; }//?: boolean;

        /// <summary>
        /// permission to control a specific room, room must be set
        /// </summary>
        public bool? RoomAdmin { get; set; }//?: boolean;

        /// <summary>
        /// name of the room, must be set for admin or join permissions
        /// </summary>
        public string Room { get; set; }//?: string;

        /// <summary>
        /// allow participant to publish. If neither canPublish or canSubscribe is set,
        /// both publish and subscribe are enabled
        /// </summary>
        public bool? CanPublish { get; set; }//?: boolean;

        /// <summary>
        /// allow participant to subscribe to other tracks 
        /// </summary>
        public bool? CanSubscribe { get; set; }//?: boolean;

        /// <summary>
        /// allow participants to publish data, defaults to true if not set
        /// </summary>
        public bool? CanPublishData { get; set; }//?: boolean;

        /// <summary>
        /// participant isn't visible to others
        /// </summary>
        public bool? Hidden { get; set; }//?: boolean;

        /// <summary>
        /// participant is recording the room, when set, allows room to indicate it's being recorded
        /// </summary>
        public bool? Recorder { get; set; }//?: boolean;
    }

    /** @internal */
    public class ClaimGrants
    {
        public string Name { get; set; }//?: string;
        public VideoGrant Video { get; set; }//?: VideoGrant;
        public string Metadata { get; set; }//?: string;
        public string Sha256 { get; set; }//?: string;
    }
}
