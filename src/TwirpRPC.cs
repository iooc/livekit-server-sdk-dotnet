﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace Livekit
{
    internal static class Util
    {
        internal const string livekitPackage = "livekit";
        internal const string defaultPrefix = "/twirp";
        internal const string protobufPackage = "livekit";
    }
    public interface Rpc
    {
        Task<string> Request<T>(string service, string method, T data, Dictionary<string, string> headers);
    }

    /**
     * JSON based Twirp V7 RPC
     */
    public class TwirpRpc : Rpc
    {
        public string host { get; set; }//: string;

        public string pkg { get; set; }//: string;

        public string prefix { get; set; }//: string;

        public HttpClient instance;

        public TwirpRpc(string host, string pkg, string prefix = null)
        {
            this.host = host;
            this.pkg = pkg;
            this.prefix = prefix ?? Util.defaultPrefix;
            this.instance = new HttpClient();
        }

        public async Task<string> Request<T>(string service, string method, T data, Dictionary<string,string> headers)
        {
            var path = $"{this.prefix}/{this.pkg}.{service}/{method}";
            foreach (var dic in headers)
                instance.DefaultRequestHeaders.Add(dic.Key, dic.Value);
            var res = await instance.PostAsJsonAsync($"http://{this.host}/{path}", data);
            return await res.Content.ReadAsStringAsync();
            //    return new Promise<any>((resolve, reject) => {
            //      const path = `${this.prefix
            //    }/${this.pkg}.${service
            //}/${ method}`;
            //this.instance
            //  .post(path, data, { headers })
            //        .then((res) => {
            //         resolve(camelcaseKeys(res.data, { deep: true }));
            //        })
            //        .catch (reject);
            //});
        }
    }
}
