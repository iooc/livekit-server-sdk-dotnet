﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Livekit
{
    public interface AccessTokenOptions
    {
        /// <summary>
        /// amount of time before expiration
        /// expressed in seconds or a string describing a time span zeit/ms.
        /// eg: '2 days', '10h', or seconds as numeric value
        /// </summary>
        int? Ttl { get; set; }//?: number | string;

        /// <summary>
        /// display name for the participant, available as `Participant.name`
        /// </summary>
        string Name { get; set; }//?: string;

        /// <summary>
        /// identity of the user, required for room join tokens
        /// </summary>
        string Identity { get; set; }//?: string;

        /// <summary>
        /// custom metadata to be passed to participants
        /// </summary>
        string Metadata { get; set; }//?: string;
    }
    public class AccessToken
    {
        // 6小时
        private const int defaultTTL = 6 * 60 * 60;
        private string apiKey;//: string;

        private string apiSecret;//: string;

        private ClaimGrants grants;//: ClaimGrants;

        public string Identity { get; set; }//?: string;

        public int Ttl { get; set; }//?: number | string;

        /**
         * Creates a new AccessToken
         * @param apiKey API Key, can be set in env LIVEKIT_API_KEY
         * @param apiSecret Secret, can be set in env LIVEKIT_API_SECRET
         */
        public AccessToken(string apiKey, string apiSecret, AccessTokenOptions options)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
            {
                apiKey = Environment.GetEnvironmentVariable("LIVEKIT_API_KEY");//process.env.LIVEKIT_API_KEY;
            }
            if (string.IsNullOrWhiteSpace(apiSecret))
            {
                apiSecret = Environment.GetEnvironmentVariable("LIVEKIT_API_SECRET");//process.env.LIVEKIT_API_SECRET;
            }
            if (string.IsNullOrWhiteSpace(apiKey) ||
                string.IsNullOrWhiteSpace(apiSecret))
            {
                throw new Exception("api-key and api-secret must be set");
            }

            this.apiKey = apiKey;
            this.apiSecret = apiSecret;
            grants = new ClaimGrants();
            Identity = options?.Identity;
            Ttl = options?.Ttl ?? defaultTTL;
            if (!string.IsNullOrWhiteSpace(options?.Metadata))
                Metadata = options.Metadata;
            
            if (!string.IsNullOrWhiteSpace(options?.Name))
                Name = options.Name;
        }

        /// <summary>
        /// Adds a video grant to this token.
        /// </summary>
        /// <param name="grant"></param>
        public void AddGrant(VideoGrant grant) => grants.Video = grant;

        /// <summary>
        /// Set metadata to be passed to the Participant, used only when joining the room
        /// </summary>
        public string Metadata { set { grants.Metadata = value; } }

        public string Name { set { grants.Name = value; } }

        public string Sha256
        {
            get
            {
                return grants.Sha256;
            }
            set
            {
                grants.Sha256 = value;
            }
        }

        /// <summary>
        /// @returns JWT encoded token
        /// </summary>
        /// <returns></returns>
        public string toJwt()
        {
            // TODO: check for video grant validity
            var handler = new JwtSecurityTokenHandler();

            var now = DateTime.Now;
            var key = Encoding.UTF8.GetBytes(apiSecret);
            var opts = new SecurityTokenDescriptor
            {
                Issuer = this.apiKey,
                Expires = now.AddSeconds(Ttl),
                NotBefore = now,
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key),
                SecurityAlgorithms.Sha256)
            };
            if (!string.IsNullOrWhiteSpace(this.Identity))
            {
                opts.Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, Identity),
                    new Claim(JwtRegisteredClaimNames.Jti, Identity)
                });
                //opts.jwtid = this.identity;
                if (!string.IsNullOrWhiteSpace(grants.Name))
                    opts.Subject.AddClaim(new Claim(JwtRegisteredClaimNames.Name, grants.Name));
                if (grants.Video != null)
                    opts.Subject.AddClaim(new Claim("video", JsonExtensions.SerializeToJson(grants.Video)));
                if (!string.IsNullOrWhiteSpace(grants.Metadata))
                    opts.Subject.AddClaim(new Claim("metadata", grants.Metadata));
                if (!string.IsNullOrWhiteSpace(grants.Sha256))
                    opts.Subject.AddClaim(new Claim("sha256", grants.Sha256));
            }
            else if (grants.Video.RoomJoin.HasValue &&
                        grants.Video.RoomJoin.Value)
                throw new Exception("identity is required for join but not set");
            
            var jwt = handler.CreateJwtSecurityToken(opts);
            return handler.WriteToken(jwt);
            //return jwt.sign(this.grants, this.apiSecret, opts);
        }
    }
    public class TokenVerifier
    {
        private string apiKey;

        private string apiSecret;

        public TokenVerifier(string apiKey, string apiSecret)
        {
            this.apiKey = apiKey;
            this.apiSecret = apiSecret;
        }

        public ClaimGrants Verify(string token) {
            var handler = new JwtSecurityTokenHandler();
            var param = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(apiSecret)),
                ValidIssuer = apiKey,
                //ValidateIssuer = false,
                //ValidateAudience = false,
                //ValidateLifetime = true,
                //ClockSkew = System.TimeSpan.Zero
            };
            var decoded = handler.ValidateToken(token, param, out SecurityToken securityToken);
            var grants = new ClaimGrants();
            foreach (var claim in decoded.Claims)
            {
                switch (claim.Type)
                {
                    case "JwtRegisteredClaimNames.Name":
                        grants.Name=claim.Value;
                        break;
                    case "video":
                        grants.Video = JsonExtensions.DeserializeFromJson<VideoGrant>(claim.Value);
                        break;
                    case "metadata":
                        grants.Metadata = claim.Value;
                        break;
                    case "sha256":
                        grants.Sha256 = claim.Value;
                        break;
                }
            }

            return grants;

            //var decoded = jwt.verify(token, this.apiSecret, { issuer: this.apiKey });
            //if (!decoded)
            //{
            //    throw new Exception("invalid token");
            //}

            //return decoded as ClaimGrants;
        }
    }
}
