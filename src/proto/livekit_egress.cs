﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;

namespace Livekit
{
    public enum EncodedFileType
    {
        /** DEFAULT_FILETYPE - file type chosen based on codecs */
        DEFAULT_FILETYPE = 0,
        MP4 = 1,
        OGG = 2,
        UNRECOGNIZED = -1,
    }

    public enum StreamProtocol
    {
        /** DEFAULT_PROTOCOL - protocol chosen based on urls */
        DEFAULT_PROTOCOL = 0,
        RTMP = 1,
        UNRECOGNIZED = -1,
    }

    public static class Proto
    {
        public static EncodedFileType EncodedFileTypeFromJSON(string @object)
        {
            switch (@object)
            {
                case "0":
                case "DEFAULT_FILETYPE":
                    return EncodedFileType.DEFAULT_FILETYPE;
                case "1":
                case "MP4":
                    return EncodedFileType.MP4;
                case "2":
                case "OGG":
                    return EncodedFileType.OGG;
                case "-1":
                case "UNRECOGNIZED":
                default:
                    return EncodedFileType.UNRECOGNIZED;
            }
        }

        public static string encodedFileTypeToJSON(EncodedFileType @object)
        {
            switch (@object)
            {
                case EncodedFileType.DEFAULT_FILETYPE:
                    return "DEFAULT_FILETYPE";
                case EncodedFileType.MP4:
                    return "MP4";
                case EncodedFileType.OGG:
                    return "OGG";
                default:
                    return "UNKNOWN";
            }
        }
    }

    public interface EncodedFileOutput
    {
        /** (optional) */
       EncodedFileType FileType { get; set; }
  /** (optional) */
 string Filepath { get; set; }
        S3Upload S3 { get; set; }//?:  | undefined;
        GCPUpload Gcp { get; set; }//?:  | undefined;
        AzureBlobUpload Azure { get; set; }//?:  | undefined;
    }

    public interface SegmentedFileOutput
    {
        /** (optional) */
       SegmentedFileProtocol protocol{ get; set; }
    /** (optional) */
    string FilenamePrefix { get; set; }
        /** (optional) */
        string PlaylistName { get; set; }
        /** (optional) */
        int SegmentDuration { get; set; }//: number;
        S3Upload  S3 { get; set; }//?:  | undefined;
        GCPUpload  Gcp { get; set; }//?:  | undefined;
        AzureBlobUpload Azure { get; set; }//?:  | undefined;
    }

    public interface StreamOutput
    {
        /** required */
       StreamProtocol Protocol { get; set; }
        /** required */
        string[] Urls { get; set; }
    }

    public class S3Upload  {
  encode(message: S3Upload, writer: _m0.Writer = _m0.Writer.create()) : _m0.Writer {
    if (message.accessKey !== '') {
      writer.uint32(10).string (message.accessKey);
    }
if (message.secret !== '')
{
    writer.uint32(18).string(message.secret);
}
if (message.region !== '')
{
    writer.uint32(26).string(message.region);
}
if (message.endpoint !== '')
{
    writer.uint32(34).string(message.endpoint);
}
if (message.bucket !== '')
{
    writer.uint32(42).string(message.bucket);
}
return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length ?: number): S3Upload
{
    const reader = input instanceof _m0.Reader? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseS3Upload();
    while (reader.pos < end)
    {
        const tag = reader.uint32();
        switch (tag >>> 3)
        {
            case 1:
                message.accessKey = reader.string();
                break;
            case 2:
                message.secret = reader.string();
                break;
            case 3:
                message.region = reader.string();
                break;
            case 4:
                message.endpoint = reader.string();
                break;
            case 5:
                message.bucket = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
        }
    }
    return message;
},

  fromJSON(object: any): S3Upload
{
    return {
    accessKey: isSet(object.accessKey) ? String(object.accessKey) : '',
      secret: isSet(object.secret) ? String(object.secret) : '',
      region: isSet(object.region) ? String(object.region) : '',
      endpoint: isSet(object.endpoint) ? String(object.endpoint) : '',
      bucket: isSet(object.bucket) ? String(object.bucket) : '',
    };
},

  toJSON(message: S3Upload): unknown
{
    const obj: any = { };
    message.accessKey !== undefined && (obj.accessKey = message.accessKey);
    message.secret !== undefined && (obj.secret = message.secret);
    message.region !== undefined && (obj.region = message.region);
    message.endpoint !== undefined && (obj.endpoint = message.endpoint);
    message.bucket !== undefined && (obj.bucket = message.bucket);
    return obj;
},

  fromPartial < I extends Exact<DeepPartial<S3Upload>, I>>(object: I): S3Upload
{
    const message = createBaseS3Upload();
    message.accessKey = object.accessKey ?? '';
    message.secret = object.secret ?? '';
    message.region = object.region ?? '';
    message.endpoint = object.endpoint ?? '';
    message.bucket = object.bucket ?? '';
    return message;
},
}
}
